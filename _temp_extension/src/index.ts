import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { requestAPI } from './handler';

/**
 * Initialization data for the @enki-portal/jupyterlab-gitlab extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: '@enki-portal/jupyterlab-gitlab:plugin',
  autoStart: true,
  activate: (app: JupyterFrontEnd) => {
    console.log(
      'JupyterLab extension @enki-portal/jupyterlab-gitlab is activated!'
    );

    requestAPI<any>('get_example')
      .then(data => {
        console.log(data);
      })
      .catch(reason => {
        console.error(
          `The jupyterlab-gitlab-msg server extension appears to be missing.\n${reason}`
        );
      });
  }
};

export default extension;
